# -*- coding: utf-8 -*
import os  

# [Content] XML Footer Text
def test_hasHomePageNode():
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('首頁') != -1

# [Behavior] Tap the coordinate on the screen
def test_tapSidebar():
	os.system('adb shell input tap 100 100')

# 1. [Content] Side Bar Text

def test_hasSideBarText():
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('查看商品分類') != -1
	assert xmlString.find('查訂單/退訂退款') != -1
	assert xmlString.find('追蹤/買過/看過清單') != -1
	assert xmlString.find('智慧標籤') != -1
	assert xmlString.find('其他') != -1
	assert xmlString.find('PChome 旅遊') != -1
	assert xmlString.find('線上手機回收') != -1
	assert xmlString.find('給24h購物APP評分') != -1

# 2. [Screenshot] Side Bar Text
def test_screencapSideBarText():
	os.system('adb shell screencap -p /sdcard/SideBarTextScreen.png')
	os.system('adb pull /sdcard/SideBarTextScreen.png')

# 3. [Context] Categories

def test_hasCategories():
	os.system('adb shell input tap 1000 100')
	os.system('adb shell input swipe 500 1300 500 100')
	os.system('adb shell input tap 1000 350')
	os.system('adb shell input tap 1000 350')
	os.system('adb shell input tap 1000 350')
	os.system('adb shell sleep 2')
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('美妝') != -1



# 4. [Screenshot] Categories

def test_screencapCategories():

	os.system('adb shell screencap -p /sdcard/CategoriesScreen.png')
	os.system('adb pull /sdcard/CategoriesScreen.png')


# 5. [Context] Categories page

def test_hasCategoriesPage():
	os.system('adb shell input tap 400 1700')
	os.system('adb shell sleep 2')
	os.system('adb shell input tap 400 1700')
	os.system('adb shell sleep 2')
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('美妝') != -1

# 6. [Screenshot] Categories page

def test_screencapCategoriesPage():

	os.system('adb shell screencap -p /sdcard/CategoriesPageScreen.png')
	os.system('adb pull /sdcard/CategoriesPageScreen.png')


# 7. [Behavior] Search item “switch”

def test_searchItemSwitch():
	os.system('adb shell input tap 1000 100')
	os.system('adb shell sleep 1')
	os.system('adb shell input text "switch" ')
	os.system('adb shell sleep 1')
	os.system('adb shell input keyevent "KEYCODE_ENTER"')
	os.system('adb shell sleep 1')

# 8. [Behavior] Follow an item and it should be add to the list

def test_followAnItem():
	os.system('adb shell input tap 1000 500')
	os.system('adb shell sleep 2')
	os.system('adb shell input tap 100 1700')
	os.system('adb shell sleep 2')
	os.system('adb shell input tap 100 100')
	os.system('adb shell sleep 2')
	os.system('adb shell input tap 100 1700')
	os.system('adb shell sleep 2')
	os.system('adb shell input tap 100 100')
	os.system('adb shell sleep 2')
	os.system('adb shell input tap 100 800')
	os.system('adb shell sleep 2')
	os.system('adb shell input swipe 500 500 500 1500')
	os.system('adb shell sleep 2')
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('Switch') != -1


# 9. [Behavior] Navigate to the detail of item

def test_navigateToTheDetail():

	os.system('adb shell input tap 200 1000')
	os.system('adb shell sleep 1')
	os.system('adb shell input tap 540 100')
	os.system('adb shell sleep 4')

# 10. [Screenshot] Disconnetion Screen

def test_disconnetion():
	os.system('adb shell input tap 100 100')
	os.system('adb shell sleep 1')
	os.system('adb shell input tap 100 100')
	os.system('adb shell sleep 1')
	os.system('adb shell svc wifi disable')
	os.system('adb shell svc data disable')
	os.system('adb shell sleep 3')
	os.system('adb shell input tap 540 1700')
	os.system('adb shell sleep 1')
	os.system('adb shell screencap -p /sdcard/disconnetion.png')
	os.system('adb pull /sdcard/disconnetion.png')



